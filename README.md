# README #

README for technical test for Substantive Research.

### What is this repository for? ###

* Quick summary
  * Taken the instructions from the given document linked below to take a csv file and process it to output a graph.
  * I have used HTML, JQUERY, and PHP
  * Used Google Charts for the graph
* [Test Online Document](https://docs.google.com/document/d/1CdFGbXfi52QEP_ae8LC-blOVsNvjSzGvEXB9vSeoLZg/edit)

### How do I get set up? ###

* Used a simple apache server (XAMPP)

### How to get results ###

* Decided to use a drop down form, so for future interaction files can be added amd see what the result is for each.
* Press the 'Draw Graph' button to see interactions per sector in percentage vale