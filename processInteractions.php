<?php
    if (isset($_REQUEST) && !empty($_REQUEST)) {
        $row = 1;
        $interactions = [];
        if (($handle = fopen($_REQUEST['csv'], 'r')) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if($row != 1){
                    $sectorId = $data[0];
                    $sectorName = $data[1];
                    $interactionDate = $data[2];

                    //if there is no interactions added for that sector
                    if(empty($interactions[$sectorId])){
                        $interactions[$sectorId] = array('sector_id'=>$sectorId, 'sectorName'=>$sectorName, 'interactionCount'=>1);
                    }
                    //if there are interactions for that sector
                    else{
                        $interactionCount = $interactions[$sectorId]['interactionCount'];
                        $interactionCount++;
                        $interactions[$sectorId] = array('sector_id'=>$sectorId, 'sectorName'=>$sectorName, 'interactionCount'=>$interactionCount);
                    }
                }
                $row++;
            }

            $outputArray = array('overallInteractions' => $row-1, 'interactions' => $interactions);

            fclose($handle);
            echo json_encode($outputArray);
        }
    }