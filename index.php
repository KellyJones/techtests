<!doctype html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});

        function drawChart(chartInteractions) {

            var data = google.visualization.arrayToDataTable(chartInteractions);

            var options = {
                title: 'Interactions by Sector',
                is3D: true,
            };

            data.sort([{column: 1}]);

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }
    </script>
</head>

<body>
<form id="getInteractions" method="post">
    <label for="interactionsCSV">Choose interactions file:</label><br>
    <select name="interactionsCSV" id="interactionsCSV">
        <option value="interactions.csv">Interactions: 2022-03-14</option>
    </select>
    <br><br>
    <input type="submit" value="Draw Graph">
</form>
<br><br>
<div id="piechart" style="width: 900px; height: 500px;"></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#getInteractions').submit(function(e) {
            e.preventDefault();

            let data = {
                'csv' : $('#interactionsCSV').val()
            }

            $.ajax({
                type: 'POST',
                url: 'processInteractions.php',
                data: data,
                success: function(response)
                {
                    let interactionsBySector = JSON.parse( response ).interactions;

                    let chartInteractions = [['Sector', 'Interactions']]

                    for(var i in interactionsBySector) {
                        chartInteractions.push([interactionsBySector[i].sectorName, interactionsBySector[i].interactionCount]);
                    }

                    drawChart(chartInteractions);
                }
            });
        });
    });
</script>
</body>
</html>

